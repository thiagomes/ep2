package ep2;

public class Van extends Veiculos {
	public Van() {
		setCombustivel((float)3.869);
		setRendimento((float)10.000);
		setRendimentoReduzido((float)0.0001);
		setCargaMax((float)3500.000);
		setVelMedia((float)80.000);	
	}
}
