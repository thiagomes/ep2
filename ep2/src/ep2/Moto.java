package ep2;

public class Moto extends Veiculos {
	
	public Moto() {
		setCombustivela((float)3.499);//para alcool
		setCombustivel((float)4.449);//para gasolina
		setRendimentoa((float)43.000);//para alcool
		setRendimento((float)50.000);//para gasolina
		setRendimentoReduzidoa((float)0.4);//para alcool
		setRendimentoReduzido((float)0.3); //para gasolina
		setCargaMax((float)50);
		setVelMedia((float)110.000);	
		}
	public float custoOperacao(String distanciaPercorrida,String pesoCarga,String margemLucro) {

		float distanciaPercorrida1= Float.parseFloat(distanciaPercorrida);
		float pesoCarga1= Float.parseFloat(pesoCarga);
		float gasolina;//custo de operação com a gasolina
		float alcool;//custo de operação com o alcool
		float margemLucro1=Float.parseFloat(margemLucro);
		float margemLucrop=margemLucro1/100; //transforma a porcentagem
		float margemLucroReal;//quanto vale a margem de lucro em relaçao ao custo de operação para gasolina
		float margemLucroReala;//quanto vale a margem de lucro em relaçao ao custo de operação para alcool
		gasolina= getCombustivel()*(distanciaPercorrida1/(getRendimento()-(getRendimentoReduzido()*pesoCarga1)));
		alcool= getCombustivela()*(distanciaPercorrida1/(getRendimentoa()-(getRendimentoReduzidoa()*pesoCarga1)));
		margemLucroReal=margemLucrop*gasolina;
		margemLucroReala=margemLucrop*alcool;
		if(gasolina>alcool) {
			
			return alcool+margemLucroReala;
		}
		else {
			
			return gasolina+margemLucroReal;
		}
			
	}
}
