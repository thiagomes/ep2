package ep2;

public class Carro extends Veiculos {
	
	public Carro() {
		setCombustivel((float)4.449); //para gasolina
		setCombustivela((float)3.499); //para alcool
		setRendimento((float)14.000); //para gasolina
		setRendimentoa((float)12.000); //para alcool
		setRendimentoReduzido((float)0.025); //para gasolina
		setRendimentoReduzidoa((float)0.0231); //para alcool
		setCargaMax((float)360);
		setVelMedia((float)100.000);	
		}
	public float custoOperacao(String distanciaPercorrida,String pesoCarga,String margemLucro) {
		
		float distanciaPercorrida1= Float.parseFloat(distanciaPercorrida);
		float pesoCarga1= Float.parseFloat(pesoCarga);
		float margemLucro1=Float.parseFloat(margemLucro);
		float margemLucrop=margemLucro1/100; //transforma a porcentagem
		float margemLucroReal;//quanto vale a margem de lucro em relaçao ao custo de operação para gasolina
		float margemLucroReala;//quanto vale a margem de lucro em relaçao ao custo de operação para alcool
		float gasolina;//custo de operação com a gasolina
		float alcool;//custo de operação com o alcool
		gasolina= getCombustivel()*(distanciaPercorrida1/(getRendimento()-(getRendimentoReduzido()*pesoCarga1)));
		alcool= getCombustivela()*(distanciaPercorrida1/(getRendimentoa()-(getRendimentoReduzidoa()*pesoCarga1)));
		margemLucroReal=margemLucrop*gasolina;
		margemLucroReala=margemLucrop*alcool;
		if(gasolina>alcool) {
			
			return alcool+margemLucroReala;
		}
		else{
			
			return gasolina+margemLucroReal;
	}
}
}
