package ep2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

public  class Veiculos {	
		private float combustivel;//para a gasolina e disel
		private float combustivela;//para o alcool
		private float rendimento;//para a gasolina e disel
		private float rendimentoa;//para o alcool
		private float carga_Max;
		private float rendimento_Reduzido; //para gasolina e disel
		private float rendimento_Reduzidoa;//para alcool
		private float velmedia;
		private String quantidadeVeiculo;
		//private	DecimalFormat format1 = new DecimalFormat("0.00");
		
		
		//métodos set e get
		public void setCombustivel(float combustivel) {
			this.combustivel = combustivel;
		}
		public float getCombustivel() {
			return combustivel;
		}
		
		public void setCombustivela(float combustivela) {
			this.combustivela = combustivela;
		}
		public float getCombustivela() {
			return combustivela;
		}
		
		public void setRendimento(float rendimento) {
			this.rendimento = rendimento;
		}
		public float getRendimento() {
			return rendimento;
		}
		
		public void setRendimentoa(float rendimentoa) {
			this.rendimentoa = rendimentoa;
		}
		public float getRendimentoa() {
			return rendimentoa;
		}
		
		public void setCargaMax(float carga_Max) {
			this.carga_Max = carga_Max;
		}
		public float getCargaMax() {
			return carga_Max;
		}
		
		public void setRendimentoReduzido(float rendimento_Reduzido) {
			this.rendimento_Reduzido = rendimento_Reduzido;
		}
		public float getRendimentoReduzido() {
			return rendimento_Reduzido;
		}
		
		public void setRendimentoReduzidoa(float rendimento_Reduzidoa) {
			this.rendimento_Reduzidoa = rendimento_Reduzidoa;
		}
		public float getRendimentoReduzidoa() {
			return rendimento_Reduzidoa;
		}
		
		public void setVelMedia(float velmedia) {
			this.velmedia = velmedia;
		}
		public float getVelMedia() {
			return velmedia;
		}
		
		//Métodos Gerais para todos os veiculos
		
		public float calcTempo(String distanciaPercorrida) {
		
			float distanciaPercorrida1=	Float.parseFloat(distanciaPercorrida);
			return distanciaPercorrida1/getVelMedia();
		}	
		
		public float custoOperacao(String distanciaPercorrida,String pesoCarga,String margemLucro) {
			float distanciaPercorrida1= Float.parseFloat(distanciaPercorrida);
			float pesoCarga1= Float.parseFloat(pesoCarga);
			float margemLucro1=Float.parseFloat(margemLucro);
			float margemLucrop=margemLucro1/100; //transforma a porcentagem
			float margemLucroReal;//quanto vale a margem de lucro em relaçao ao custo de operação
			margemLucroReal=margemLucrop*(getCombustivel()*(distanciaPercorrida1/(getRendimento()-(getRendimentoReduzido()*pesoCarga1))));
			return (getCombustivel()*(distanciaPercorrida1/(getRendimento()-(getRendimentoReduzido()*pesoCarga1))))+margemLucroReal;	
		}
		public float custoBeneficio(String distanciaPercorrida, String pesoCarga,String margemLucro) {
			
			return	calcTempo(distanciaPercorrida)/custoOperacao(distanciaPercorrida,pesoCarga,margemLucro);
			
			
		}
		
		public boolean RealizaEntrega(String tempMax,String pesoCarga,String distanciaPercorrida,String quantidade) {
			float quantidade1=Float.parseFloat(quantidade);
			float pesoCarga1= Float.parseFloat(pesoCarga);
			float tempMax1= Float.parseFloat(tempMax);
			if(calcTempo(distanciaPercorrida)<=tempMax1 && pesoCarga1<=getCargaMax() && 0<quantidade1) { 
				return true;
			}
			else {
				return false;
			}
			
		}
		public String LerArquivo(String caminho) { //retorna a quantidade de veiculo que esta no arquivo
			String conteudoarquivo="";
			try {
				FileReader arquivo= new FileReader(caminho);
				BufferedReader lerArq= new BufferedReader(arquivo);
				String linha="";
				try {
					linha=lerArq.readLine();
					while(linha!=null) {
						conteudoarquivo+=linha;
						linha=lerArq.readLine();
					}
					arquivo.close();
				}catch (IOException ex) {
					conteudoarquivo="Erro: Não foi possivel ler o arquivo";
				}
			}catch (FileNotFoundException ex) {
				conteudoarquivo="Erro: arquivo não encontrado";
				
			}
			if(conteudoarquivo.contains("Erro")) {
				return "";
			}
			else return conteudoarquivo;
			
		}
		public 	void EscreveArquivo(String caminho,String quantidadeVeiculos) {   //guarda no arquivo a quantidade de veiculos digitada pelo usuario
			try {
				FileWriter arquivo= new FileWriter(caminho);
				PrintWriter gravarArq= new PrintWriter(arquivo);
				gravarArq.println(quantidadeVeiculos);
				gravarArq.close();
				
			}catch (IOException e) {
				System.out.println(e.getMessage());

			}
		}
		public String DiminuiVeiculo(String caminho) {
			int transforma= Integer.parseInt(LerArquivo(caminho)); //ela transforma a quantidade de veiculos que esta no arquivo em int
			transforma--; //diminui 1 veiculo 
			String quantidadeDiminuida = String.valueOf(transforma); // transforma em string a quantidade de veiculos agora decrementada
			EscreveArquivo(caminho,quantidadeDiminuida); //joga pra dentro do arquivo denovo
			return quantidadeDiminuida;
		}
		public String getQuantidadeVeiculo() {
			return quantidadeVeiculo;
		}
		public void setQuantidadeVeiculo(String quantidadeVeiculo) {
			this.quantidadeVeiculo = quantidadeVeiculo;
		}

}
