package interfacegrafica;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import ep2.Carreta;
import ep2.Carro;
import ep2.Moto;
import ep2.Van;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.swing.JScrollBar;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaResultado extends JFrame {

	private JPanel contentPane;
	private ButtonGroup bg = new ButtonGroup();
	private String mostraMelhorCustoBeneficio=null;
	private String mostraMenorCusto=null;
	private String mostraMenorTempo=null;
	private Carro carro =new Carro();
	private Moto moto = new Moto();
	private Van van = new Van();
	private Carreta carreta = new Carreta();
	private boolean verificaCarro=false; //vai ser usada como parametro para saber se o arquivo foi modificado ou não na quantidade de veiculos
	private boolean verificaMoto=false;
	private boolean verificaVan=false;
	private boolean verificaCarreta=false;
	private boolean recebeCarroRealizaEntrega; //variaveis para receber da classe Tela se os veiculos podem realizar a entrega
	private boolean recebeMotoRealizaEntrega;
	private boolean recebeVanRealizaEntrega;
	private boolean recebeCarretaRealizaEntrega;
	private JLabel lblMostraresultado = new JLabel();


	

	

	public void recebendo(String MenorCusto,String MenorTempo,String MelhorCustoBeneficio,boolean carroRealizaEntrega,boolean motoRealizaEntrega,boolean vanRealizaEntrega,boolean carretaRealizaEntrega){
	
		lblMostraresultado.setText("<html>Menor Custo: "+MenorCusto+"<html><br><br>Menor Tempo: "+MenorTempo+"<html><br><br>Melhor Custo Beneficio: "+MelhorCustoBeneficio);
		
		
		setMostraMelhorCustoBeneficio(MelhorCustoBeneficio);
		setMostraMenorCusto(MenorCusto);
		setMostraMenorTempo(MenorTempo);
		recebeCarroRealizaEntrega=carroRealizaEntrega;
		recebeMotoRealizaEntrega=motoRealizaEntrega;
		recebeVanRealizaEntrega=vanRealizaEntrega;
		recebeCarretaRealizaEntrega=carretaRealizaEntrega;
		
	}
	/**
	 * Create the frame.
	 */
	public TelaResultado() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 474, 359);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		
		JRadioButton rdbtnMenorTempo = new JRadioButton("Menor Tempo");
		rdbtnMenorTempo.setBounds(227, 112, 149, 23);
		contentPane.add(rdbtnMenorTempo);
		
		JRadioButton rdbtnMenorCusto = new JRadioButton("Menor Custo");
		rdbtnMenorCusto.setBounds(227, 139, 149, 23);
		contentPane.add(rdbtnMenorCusto);
		
		JRadioButton rdbtnMelhorCustoBenefcio = new JRadioButton("Melhor Custo Benefício");
		rdbtnMelhorCustoBenefcio.setBounds(227, 166, 188, 23);
		contentPane.add(rdbtnMelhorCustoBenefcio);
		
		JRadioButton rdbtnCarro = new JRadioButton("Carro");
		rdbtnCarro.setBounds(227, 193, 149, 23);
		contentPane.add(rdbtnCarro);
		
		JRadioButton rdbtnMoto = new JRadioButton("Moto");
		rdbtnMoto.setBounds(227, 220, 149, 23);
		contentPane.add(rdbtnMoto);
		
		JRadioButton rdbtnVan = new JRadioButton("Van");
		rdbtnVan.setBounds(227, 250, 149, 23);
		contentPane.add(rdbtnVan);
		
		JRadioButton rdbtnCarreta = new JRadioButton("Carreta");
		rdbtnCarreta.setBounds(227, 278, 149, 23);
		contentPane.add(rdbtnCarreta);
		
		bg.add(rdbtnMelhorCustoBenefcio);
		bg.add(rdbtnMenorCusto);
		bg.add(rdbtnMenorTempo);
		bg.add(rdbtnCarro);
		bg.add(rdbtnMoto);
		bg.add(rdbtnVan);
		bg.add(rdbtnCarreta);
		
		JLabel lblescolhaAOpo = new JLabel("<html>Escolha a opção que melhor atenda aos interesses de sua viagem");
		lblescolhaAOpo.setBounds(227, 0, 149, 120);
		contentPane.add(lblescolhaAOpo);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if(rdbtnMelhorCustoBenefcio.isSelected()) {
					//Escolher o veiculo que tem o Melhor Custo Beneficio e subtrair 1 na sua quantidade
					if(getMostraMelhorCustoBeneficio().contains("Carro")){
						
						carro.setQuantidadeVeiculo(carro.DiminuiVeiculo("src/ep2/quantidadeCarro.txt"));
						verificaCarro=true;
						setVerificaCarro(verificaCarro);
					
					}
					if(getMostraMelhorCustoBeneficio().contains("Moto")){
						
						moto.setQuantidadeVeiculo(moto.DiminuiVeiculo("src/ep2/quantidadeMoto.txt"));
						verificaMoto=true;
						setVerificaMoto(verificaMoto);
					}
					if(getMostraMelhorCustoBeneficio().contains("Van")){
						
						van.setQuantidadeVeiculo(van.DiminuiVeiculo("src/ep2/quantidadeVan.txt"));
						verificaVan=true;
						setVerificaVan(verificaVan);
					}
					if(getMostraMelhorCustoBeneficio().contains("Carreta")){
						
						carreta.setQuantidadeVeiculo(carreta.DiminuiVeiculo("src/ep2/quantidadeCarreta.txt"));
						verificaCarreta=true;
						setVerificaCarreta(verificaCarreta);
					}

					dispose();
				}
				if(rdbtnMenorCusto.isSelected()) {
					//Escolher o veiculo que tem o Menor Custo e subtrair 1 na sua quantidade
					if(getMostraMenorCusto().contains("Carro")){
						
						carro.setQuantidadeVeiculo(carro.DiminuiVeiculo("src/ep2/quantidadeCarro.txt"));
						verificaCarro=true;
						setVerificaCarro(verificaCarro);
					
					}
					if(getMostraMenorCusto().contains("Moto")){
					
						moto.setQuantidadeVeiculo(moto.DiminuiVeiculo("src/ep2/quantidadeMoto.txt"));
						verificaMoto=true;
						setVerificaMoto(verificaMoto);
					}
					if(getMostraMenorCusto().contains("Van")){
						
						van.setQuantidadeVeiculo(van.DiminuiVeiculo("src/ep2/quantidadeVan.txt"));
						verificaVan=true;
						setVerificaVan(verificaVan);
					}
					if(getMostraMenorCusto().contains("Carreta")){
						
						carreta.setQuantidadeVeiculo(carreta.DiminuiVeiculo("src/ep2/quantidadeCarreta.txt"));
						verificaCarreta=true;
						setVerificaCarreta(verificaCarreta);
					}

					dispose();
				}
				if(rdbtnMenorTempo.isSelected()) {
					//Escolher o veiculo que tem o Menor Tempo e subtrair 1 na sua quantidade
					if(getMostraMenorTempo().contains("Carro")){
					
						carro.setQuantidadeVeiculo(carro.DiminuiVeiculo("src/ep2/quantidadeCarro.txt"));
						verificaCarro=true;
						setVerificaCarro(verificaCarro);
						
					}
					if(getMostraMenorTempo().contains("Moto")){
					
						moto.setQuantidadeVeiculo(moto.DiminuiVeiculo("src/ep2/quantidadeMoto.txt"));
						verificaMoto=true;
						setVerificaMoto(verificaMoto);
					}
					if(getMostraMenorTempo().contains("Van")){
					
						van.setQuantidadeVeiculo(van.DiminuiVeiculo("src/ep2/quantidadeVan.txt"));
						verificaVan=true;
						setVerificaVan(verificaVan);
					}
					if(getMostraMenorTempo().contains("Carreta")){
				
						carreta.setQuantidadeVeiculo(carreta.DiminuiVeiculo("src/ep2/quantidadeCarreta.txt"));
						verificaCarreta=true;
						setVerificaCarreta(verificaCarreta);
					}

					dispose();
				}
			 if(rdbtnCarro.isSelected()) {
				 if(recebeCarroRealizaEntrega) {
					 	carro.setQuantidadeVeiculo(carro.DiminuiVeiculo("src/ep2/quantidadeCarro.txt"));
						verificaCarro=true;
						setVerificaCarro(verificaCarro);
						dispose();
				 }
				 else {
					 JOptionPane.showMessageDialog(null, "Este veículo não pode realizar esta viagem");
				 }
				
			 }
			 if(rdbtnMoto.isSelected()) {
				 if(recebeMotoRealizaEntrega) {
					 	moto.setQuantidadeVeiculo(moto.DiminuiVeiculo("src/ep2/quantidadeMoto.txt"));
						verificaMoto=true;
						setVerificaMoto(verificaMoto);
						dispose();
				 }
				 else {
					 JOptionPane.showMessageDialog(null, "Este veículo não pode realizar esta viagem");
				 }
				
					
			 }	
			 if(rdbtnVan.isSelected()) {
				 if(recebeVanRealizaEntrega) {
					 	van.setQuantidadeVeiculo(van.DiminuiVeiculo("src/ep2/quantidadeVan.txt"));
						verificaVan=true;
						setVerificaVan(verificaVan);
						dispose();
				 }
				 else {
					 JOptionPane.showMessageDialog(null, "Este veículo não pode realizar esta viagem");
				 }
			
					
			 }	
			 if(rdbtnCarreta.isSelected()) {
				 if(recebeCarretaRealizaEntrega) {
					 	carreta.setQuantidadeVeiculo(carreta.DiminuiVeiculo("src/ep2/quantidadeCarreta.txt"));
						verificaCarreta=true;
						setVerificaCarreta(verificaCarreta);
						dispose();
				 }
				 else {
					 JOptionPane.showMessageDialog(null, "Este veículo não pode realizar esta viagem");
				 }

			 }	
				
				
			}
		});
		btnOk.setBounds(259, 322, 117, 25);
		contentPane.add(btnOk);
		
		lblMostraresultado = new JLabel();
		lblMostraresultado.setBounds(12, 30, 201, 290);
		contentPane.add(lblMostraresultado);
		
	}


	public String getMostraMelhorCustoBeneficio() {
		return mostraMelhorCustoBeneficio;
	}


	public void setMostraMelhorCustoBeneficio(String mostraMelhorCustoBeneficio) {
		this.mostraMelhorCustoBeneficio = mostraMelhorCustoBeneficio;
	}


	public String getMostraMenorCusto() {
		return mostraMenorCusto;
	}


	public void setMostraMenorCusto(String mostraMenorCusto) {
		this.mostraMenorCusto = mostraMenorCusto;
	}


	public String getMostraMenorTempo() {
		return mostraMenorTempo;
	}


	public void setMostraMenorTempo(String mostraMenorTempo) {
		this.mostraMenorTempo = mostraMenorTempo;
	}

	
	public boolean getVerificaCarro() {
		return verificaCarro;
	}


	public void setVerificaCarro(boolean verificaCarro) {
		this.verificaCarro = verificaCarro;
	}


	public boolean getVerificaMoto() {
		return verificaMoto;
	}


	public void setVerificaMoto(boolean verificaMoto) {
		this.verificaMoto = verificaMoto;
	}


	public boolean getVerificaVan() {
		return verificaVan;
	}


	public void setVerificaVan(boolean verificaVan) {
		this.verificaVan = verificaVan;
	}


	public boolean getVerificaCarreta() {
		return verificaCarreta;
	}


	public void setVerificaCarreta(boolean verificaCarreta) {
		this.verificaCarreta = verificaCarreta;
	}
}
