package interfacegrafica;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ep2.Carreta;
import ep2.Carro;
import ep2.Moto;
import ep2.Van;
import ep2.Veiculos;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;

public class Tela extends JFrame {

	private JPanel contentPane;
	private JTextField pesoCarga;
	private JTextField distanciaPercorrida;
	private JTextField tempMax;
	private Carro carro = new Carro();
	private Moto moto = new Moto();
	private Van van = new Van();
	private Carreta carreta = new Carreta();
	private TelaResultado janela = new TelaResultado();
	private JTextField quantidadeCarreta;
	private JTextField quantidadeVan;
	private JTextField quantidadeCarro;
	private JTextField quantidadeMoto;
	private JTextField margemLucro; //margem de lucro digitado pelo usuario
	private Veiculos margemLucroarq =new Veiculos(); //margem de lucro contida no arquivo
	private	DecimalFormat format1 = new DecimalFormat("0.00"); //para mostrar apenas 2 casas decimais 
	
	/**
	 * 
	 * Launch the application.
	 * 
	 */
	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tela frame = new Tela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	public String MenorCusto() {

		VerificaCarro(janela,quantidadeCarro.getText());
		VerificaMoto(janela,quantidadeMoto.getText());
		VerificaVan(janela,quantidadeVan.getText());
		VerificaCarreta(janela,quantidadeCarreta.getText());
		margemLucroarq.EscreveArquivo("src/ep2/margemlucro.txt", margemLucro.getText());
		
		ArrayList<Float> menorCusto =new ArrayList<Float> ();
		if(carro.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carro.LerArquivo("src/ep2/quantidadeCarro.txt"))) {
			menorCusto.add(carro.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		if(moto.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),moto.LerArquivo("src/ep2/quantidadeMoto.txt"))) {
			menorCusto.add(moto.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		if(van.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),van.LerArquivo("src/ep2/quantidadeVan.txt"))) {
			menorCusto.add(van.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		if(carreta.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carreta.LerArquivo("src/ep2/quantidadeCarreta.txt"))) {
			menorCusto.add(carreta.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		menorCusto.sort(null);//ordena o vetor em ordem crescente (do menor para o maior)
		if(menorCusto.get(0)==carro.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String minimocusto= "<html>Carro <br>Custo de Operação:R$ "+format1.format(carro.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()))+"<html><br>Tempo: "+format1.format(carro.calcTempo(distanciaPercorrida.getText()))+"h";
			
				return minimocusto;
		}
		if(menorCusto.get(0)==moto.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String minimocusto= "<html>Moto <br>Custo de Operação:R$ "+format1.format(moto.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()))+"<html><br>Tempo: "+format1.format(moto.calcTempo(distanciaPercorrida.getText()))+"h";
			
			return minimocusto;
		}
		if(menorCusto.get(0)==van.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String minimocusto= "<html>Van <br>Custo de Operação:R$ "+format1.format(van.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()))+"<html><br>Tempo: "+format1.format(van.calcTempo(distanciaPercorrida.getText()))+"h";
			
			return minimocusto;
		}
		if(menorCusto.get(0)==carreta.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String minimocusto="<html>Carreta <br>Custo de Operação:R$ "+format1.format(carreta.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()))+"<html><br>Tempo: "+format1.format(carreta.calcTempo(distanciaPercorrida.getText()))+"h";
			
			return minimocusto;
		}
		else {
			String minimocusto="Não Existe";
			;
			return minimocusto;
		}
		
	}
	
	public String MenorTempo() {
		VerificaCarro(janela,quantidadeCarro.getText());
		VerificaMoto(janela,quantidadeMoto.getText());
		VerificaVan(janela,quantidadeVan.getText());
		VerificaCarreta(janela,quantidadeCarreta.getText());
		margemLucroarq.EscreveArquivo("src/ep2/margemlucro.txt", margemLucro.getText());
		margemLucro.setText(margemLucroarq.LerArquivo("src/ep2/margemlucro.txt"));
		
		
		ArrayList<Float> menorTempo =new ArrayList<Float> ();
		if(carro.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carro.LerArquivo("src/ep2/quantidadeCarro.txt"))) {
			menorTempo.add(carro.calcTempo(distanciaPercorrida.getText()));
		}
		if(moto.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),moto.LerArquivo("src/ep2/quantidadeMoto.txt"))) {
			menorTempo.add(moto.calcTempo(distanciaPercorrida.getText()));
		}
		if(van.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),van.LerArquivo("src/ep2/quantidadeVan.txt"))) {
			menorTempo.add(van.calcTempo(distanciaPercorrida.getText()));
		}
		if(carreta.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carreta.LerArquivo("src/ep2/quantidadeCarreta.txt"))) {
			menorTempo.add(carreta.calcTempo(distanciaPercorrida.getText()));
		}
		menorTempo.sort(null);//ordena o vetor em ordem crescente (do menor para o maior)
		if(menorTempo.get(0)==carro.calcTempo(distanciaPercorrida.getText())) {
			String minimotempo= "<html>Carro <br>Tempo: "+format1.format(carro.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(carro.custoOperacao(distanciaPercorrida.getText(),
					pesoCarga.getText(),margemLucro.getText()));
				
				return minimotempo;
		}
		if(menorTempo.get(0)==moto.calcTempo(distanciaPercorrida.getText())) {
			String minimotempo= "<html>Moto <br>Tempo: "+format1.format(moto.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(moto.custoOperacao(distanciaPercorrida.getText(),
					pesoCarga.getText(),margemLucro.getText()));
				
				return minimotempo;
		}
		if(menorTempo.get(0)==van.calcTempo(distanciaPercorrida.getText())) {
			String minimotempo= "<html>Van <br>Tempo: "+format1.format(van.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(van.custoOperacao(distanciaPercorrida.getText(),
					pesoCarga.getText(),margemLucro.getText()));
				
				return minimotempo;
		}
		if(menorTempo.get(0)==carreta.calcTempo(distanciaPercorrida.getText())) {
			String minimotempo= "<html>Carreta <br>Tempo: "+format1.format(carreta.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(carreta.custoOperacao(distanciaPercorrida.getText(),
					pesoCarga.getText(),margemLucro.getText()));
				
				return minimotempo;
		}
		else {
			String minimotempo="Não Existe";
			return minimotempo;
		}
		
	}
	public String MelhorCustoBeneficio() {

		VerificaCarro(janela,quantidadeCarro.getText());
		VerificaMoto(janela,quantidadeMoto.getText());
		VerificaVan(janela,quantidadeVan.getText());
		VerificaCarreta(janela,quantidadeCarreta.getText());
		margemLucroarq.EscreveArquivo("src/ep2/margemlucro.txt", margemLucro.getText());
		margemLucro.setText(margemLucroarq.LerArquivo("src/ep2/margemlucro.txt"));
			
		ArrayList<Float> cCustoBeneficio = new ArrayList<Float> ();//cCustoBeneficio= coeficiente de custo beneficio retornado pela função CustoBeneficio
		if(carro.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carro.LerArquivo("src/ep2/quantidadeCarro.txt"))) {
			cCustoBeneficio.add(carro.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		if(moto.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),moto.LerArquivo("src/ep2/quantidadeMoto.txt"))) {
			cCustoBeneficio.add(moto.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		if(van.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),van.LerArquivo("src/ep2/quantidadeVan.txt"))) {
			cCustoBeneficio.add(van.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		if(carreta.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carreta.LerArquivo("src/ep2/quantidadeCarreta.txt"))) {
			cCustoBeneficio.add(carreta.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
		}
		cCustoBeneficio.sort(null);
		
		if(cCustoBeneficio.get(0)==carro.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String melhorcb= "<html>Carro "+"<html><br>Tempo: "+format1.format(carro.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(carro.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
				return melhorcb;
		}
		if(cCustoBeneficio.get(0)==moto.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String melhorcb= "<html>Moto "+"<html><br>Tempo: "+format1.format(moto.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(moto.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
				return melhorcb;
		}
		if(cCustoBeneficio.get(0)==van.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String melhorcb= "<html>Van "+"<html><br>Tempo: "+format1.format(van.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(van.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
				return melhorcb;
		}
		if(cCustoBeneficio.get(0)==carreta.custoBeneficio(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText())) {
			String melhorcb= "<html>Carreta "+"<html><br>Tempo: "+format1.format(carreta.calcTempo(distanciaPercorrida.getText()))+"h"+"<html><br>Custo de Operação:R$ "+format1.format(carreta.custoOperacao(distanciaPercorrida.getText(), pesoCarga.getText(),margemLucro.getText()));
				return melhorcb;
		}
		else {
			String melhorcb="Não Existe";
			return melhorcb;
		}
	}
	
	

	/**
	 * Create the frame.
	 */
//Funções que "setam" a frota conforme os veículos são atualizados tanto pelo usuario quanto pelo próprio sistema
	
	public void VerificaCarro(TelaResultado verificaCarro,String quantidadeCarros) {

			
		 if(verificaCarro.getVerificaCarro()) {
				carro.EscreveArquivo("src/ep2/quantidadeCarro.txt", carro.LerArquivo("src/ep2/quantidadeCarro.txt"));
				quantidadeCarro.setText(carro.LerArquivo("src/ep2/quantidadeCarro.txt")); 
				verificaCarro.setVerificaCarro(false);
		 }
		else {
			carro.EscreveArquivo("src/ep2/quantidadeCarro.txt", quantidadeCarro.getText());
			quantidadeCarro.setText(carro.LerArquivo("src/ep2/quantidadeCarro.txt")); 
		}

	}
	
	
	public void VerificaMoto(TelaResultado verificaMoto, String quantidadeMotos) {

		
		 if(verificaMoto.getVerificaMoto()) {
				moto.EscreveArquivo("src/ep2/quantidadeMoto.txt", moto.LerArquivo("src/ep2/quantidadeMoto.txt"));
				quantidadeMoto.setText(moto.LerArquivo("src/ep2/quantidadeMoto.txt")); 
				verificaMoto.setVerificaMoto(false);
			}
		else {
			moto.EscreveArquivo("src/ep2/quantidadeMoto.txt", quantidadeMoto.getText());
			quantidadeMoto.setText(moto.LerArquivo("src/ep2/quantidadeMoto.txt"));
		}
	
	}
	
	
	public void VerificaVan(TelaResultado verificaVan, String quantidadeVans) {

		 if(verificaVan.getVerificaVan()) {
				van.EscreveArquivo("src/ep2/quantidadeVan.txt", van.LerArquivo("src/ep2/quantidadeVan.txt"));
				quantidadeVan.setText(van.LerArquivo("src/ep2/quantidadeVan.txt")); 
				verificaVan.setVerificaVan(false);
			}

		else {
			van.EscreveArquivo("src/ep2/quantidadeVan.txt", quantidadeVan.getText());
			quantidadeVan.setText(van.LerArquivo("src/ep2/quantidadeVan.txt")); 
		}
	}
	
	
	public void VerificaCarreta(TelaResultado verificaCarreta, String quantidadeCarretas) {

		 if(verificaCarreta.getVerificaCarreta()) {
				carreta.EscreveArquivo("src/ep2/quantidadeCarreta.txt", carreta.LerArquivo("src/ep2/quantidadeCarreta.txt"));
				quantidadeCarreta.setText(carreta.LerArquivo("src/ep2/quantidadeCarreta.txt")); 
				verificaCarreta.setVerificaCarreta(false);
			}

		
		else {
			carreta.EscreveArquivo("src/ep2/quantidadeCarreta.txt", quantidadeCarreta.getText());
			quantidadeCarreta.setText(carreta.LerArquivo("src/ep2/quantidadeCarreta.txt")); 
		}
	}

	
	
	public Tela() {
		
	
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 450, 300);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Frota", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblQuantidadeDeCarreta = new JLabel("Quantidade de Carretas");
		lblQuantidadeDeCarreta.setBounds(12, 12, 189, 15);
		panel.add(lblQuantidadeDeCarreta);
		
		quantidadeCarreta = new JTextField(carreta.LerArquivo("src/ep2/quantidadeCarreta.txt")); //vai sempre iniciar com a última quantidade de carreta cadastrada
		quantidadeCarreta.addKeyListener(new KeyAdapter() { //função para aceitar apenas números 
			
			public void keyTyped(KeyEvent evt) {
				String caracteres="0987654321";
				if(!caracteres.contains(evt.getKeyChar()+"")){
				evt.consume();
				}
			}
		});
		quantidadeCarreta.setBounds(12, 39, 114, 19);
		panel.add(quantidadeCarreta);
		quantidadeCarreta.setColumns(10);
		
		
		JLabel lblQuantidadeDeVans = new JLabel("Quantidade de Vans");
		lblQuantidadeDeVans.setBounds(12, 70, 157, 15);
		panel.add(lblQuantidadeDeVans);
		
		quantidadeVan = new JTextField(van.LerArquivo("src/ep2/quantidadeVan.txt"));
		quantidadeVan.addKeyListener(new KeyAdapter() { //função para aceitar apenas números 
			
			public void keyTyped(KeyEvent evt) {
				String caracteres="0987654321";
				if(!caracteres.contains(evt.getKeyChar()+"")){
				evt.consume();
				}
			}
		});
		quantidadeVan.setBounds(12, 97, 114, 19);
		panel.add(quantidadeVan);
		quantidadeVan.setColumns(10);
	
		
		JLabel lblQuantidadeDeCarros = new JLabel("Quantidade de Carros");
		lblQuantidadeDeCarros.setBounds(12, 128, 171, 15);
		panel.add(lblQuantidadeDeCarros);
		
	    quantidadeCarro = new JTextField(carro.LerArquivo("src/ep2/quantidadeCarro.txt"));
	    quantidadeCarro.addKeyListener(new KeyAdapter() {
	    
	    	public void keyTyped(KeyEvent evt) { //função para aceitar apenas números
	    		String caracteres="0987654321";
	    		if(!caracteres.contains(evt.getKeyChar()+"")){
	    		evt.consume();
	    		}
	    	}
	    });
		quantidadeCarro.setBounds(12, 155, 114, 19);
		panel.add(quantidadeCarro);
		quantidadeCarro.setColumns(10);
	
		
		JLabel lblQuantidadeDeMotos = new JLabel("Quantidade de Motos");
		lblQuantidadeDeMotos.setBounds(12, 186, 157, 15);
		panel.add(lblQuantidadeDeMotos);
		
		quantidadeMoto = new JTextField(moto.LerArquivo("src/ep2/quantidadeMoto.txt"));
		quantidadeMoto.addKeyListener(new KeyAdapter() { //função para aceitar apenas números 
			
			public void keyTyped(KeyEvent evt) {
				String caracteres="0987654321";
				if(!caracteres.contains(evt.getKeyChar()+"")){
				evt.consume();
				}
			}
		});
		quantidadeMoto.setBounds(12, 213, 114, 19);
		panel.add(quantidadeMoto);
		quantidadeMoto.setColumns(10);
	
		
		JLabel lblMargemDeLucro = new JLabel("Margem de Lucro (%)");
		lblMargemDeLucro.setBounds(228, 12, 182, 15);
		panel.add(lblMargemDeLucro);
		
		margemLucro = new JTextField(margemLucroarq.LerArquivo("src/ep2/margemlucro.txt"));
		margemLucro.addKeyListener(new KeyAdapter() { //função para aceitar apenas números e o .
			
			public void keyTyped(KeyEvent evt) {
				String caracteres="0987654321.";
				if(!caracteres.contains(evt.getKeyChar()+"")){
				evt.consume();
				}
			}
		});
		margemLucro.setBounds(238, 39, 114, 19);
		panel.add(margemLucro);
		margemLucro.setColumns(10);
		
		
		JButton btnAtualiza = new JButton("Atualizar");
		btnAtualiza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				VerificaCarro(janela,quantidadeCarro.getText());
				VerificaMoto(janela,quantidadeMoto.getText());
				VerificaVan(janela,quantidadeVan.getText());
				VerificaCarreta(janela,quantidadeCarreta.getText());
				margemLucroarq.EscreveArquivo("src/ep2/margemlucro.txt", margemLucro.getText());
				margemLucro.setText(margemLucroarq.LerArquivo("src/ep2/margemlucro.txt"));
			}
		});
		btnAtualiza.setBounds(270, 213, 117, 25);
		panel.add(btnAtualiza);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Viagem", null, panel_1, null);
		panel_1.setLayout(null);
		
			JButton btnCalcula = new JButton("Calcular");
			btnCalcula.setBounds(22, 191, 117, 25);
			panel_1.add(btnCalcula);
			
			tempMax = new JTextField();
			tempMax.addKeyListener(new KeyAdapter() { //função para aceitar apenas números e o .
				@Override
				public void keyTyped(KeyEvent evt) {
					String caracteres="0987654321.";
					if(!caracteres.contains(evt.getKeyChar()+"")){
					evt.consume();
					}
				}
			});
			tempMax.setBounds(22, 150, 114, 19);
			panel_1.add(tempMax);
			tempMax.setColumns(10);
			
			JLabel lblTempoDeEntrega = new JLabel("Tempo de Entrega (horas)");
			lblTempoDeEntrega.setBounds(12, 123, 208, 15);
			panel_1.add(lblTempoDeEntrega);
			
			distanciaPercorrida = new JTextField();
			distanciaPercorrida.addKeyListener(new KeyAdapter() { //função para aceitar apenas números e o .
		
				public void keyTyped(KeyEvent evt) {
					String caracteres="0987654321.";
					if(!caracteres.contains(evt.getKeyChar()+"")){
					evt.consume();
					}
				}
			});
			distanciaPercorrida.setBounds(22, 92, 114, 19);
			panel_1.add(distanciaPercorrida);
			distanciaPercorrida.setColumns(10);
			
			JLabel lblDistanciakm = new JLabel("Distância (km)");
			lblDistanciakm.setBounds(12, 65, 100, 15);
			panel_1.add(lblDistanciakm);
			
			pesoCarga = new JTextField();
			pesoCarga.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent evt) { //função para aceitar apenas números e o .
					String caracteres="0987654321.";
					if(!caracteres.contains(evt.getKeyChar()+"")){
					evt.consume();
					}
				}
			});
			pesoCarga.setBounds(22, 34, 114, 19);
			panel_1.add(pesoCarga);
			pesoCarga.setColumns(10);
			
			JLabel lblPesoDaCarga = new JLabel("Peso da Carga (kg)");
			lblPesoDaCarga.setBounds(7, 12, 144, 15);
			panel_1.add(lblPesoDaCarga);
			btnCalcula.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					if(!tempMax.getText().isEmpty() && !pesoCarga.getText().isEmpty() && !distanciaPercorrida.getText().isEmpty() && !quantidadeCarro.getText().isEmpty() && !quantidadeMoto.getText().isEmpty() && !quantidadeCarreta.getText().isEmpty() && !quantidadeVan.getText().isEmpty() && !margemLucro.getText().isEmpty()) {
						if(carro.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carro.LerArquivo("src/ep2/quantidadeCarro.txt")) || moto.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),moto.LerArquivo("src/ep2/quantidadeMoto.txt")) || van.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),van.LerArquivo("src/ep2/quantidadeVan.txt")) || carreta.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carreta.LerArquivo("src/ep2/quantidadeCarreta.txt"))) {
							if(janela==null) {
								janela= new TelaResultado();
								janela.setLocationRelativeTo(null);
								janela.setVisible(true);
								janela.setResizable(false);
								janela.recebendo(MenorCusto(), MenorTempo(), MelhorCustoBeneficio(),carro.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carro.LerArquivo("src/ep2/quantidadeCarro.txt")),moto.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),moto.LerArquivo("src/ep2/quantidadeMoto.txt")),van.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),van.LerArquivo("src/ep2/quantidadeVan.txt")),carreta.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carreta.LerArquivo("src/ep2/quantidadeCarreta.txt")));
							}
							else {
								janela.setLocationRelativeTo(null);
								janela.setVisible(true);
								janela.setResizable(false);
								janela.recebendo(MenorCusto(), MenorTempo(), MelhorCustoBeneficio(),carro.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carro.LerArquivo("src/ep2/quantidadeCarro.txt")),moto.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),moto.LerArquivo("src/ep2/quantidadeMoto.txt")),van.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),van.LerArquivo("src/ep2/quantidadeVan.txt")),carreta.RealizaEntrega(tempMax.getText(), pesoCarga.getText(), distanciaPercorrida.getText(),carreta.LerArquivo("src/ep2/quantidadeCarreta.txt")));
							}
						}
						else {
							JOptionPane.showMessageDialog(null, "Não há veiculos disponiveis que possam realizar esta viagem");
						}
				}
					else {
						JOptionPane.showMessageDialog(null, "Preencha primeiro os campos indicados");
				}
						
				}
				});
	}	
}
	

